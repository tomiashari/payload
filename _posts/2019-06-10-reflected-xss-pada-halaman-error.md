---
layout: post
title: Reflected XSS Pada Halaman Error
date: 2019-04-24 13:37:00 PM
permalink: /project/2019-06-10-reflected-xss-pada-halaman-error/
author: xhellc0de
categories: [ pentesting, xss, ]
tags: vulnerability
comments: true
image: /images/xss_pic.png
---

![XSS on Error Page](/images/xss_pic.png)

Terkadang untuk melakukan eksploitasi XSS (khususnya Reflected XSS), kita terfokus untuk menemukan halaman input seperti "Kolom Pencarian" dan sejenisnya untuk dilakukan pengetesan untuk mengetahui apakah kolom tersebut memiliki kerentanan terhadap serangan XSS atau tidak.

Tak jarang seorang developer hanya fokus untuk melakukan sanitasi dan filter terhadap serangan ini pada halaman yang lazim di akses oleh pengunjung saya. Padahal tidak menutup kemungkinan serangan XSS dapat terjadi pada halaman lainnya, termasuk "Halaman Error".

Pada saat melakukan Bug Hunting, penulis menemukan sebuah fitur upload dan download file. Ketika file berhasil di upload, untuk melakukan download file tersebut user akan di arahkan ke URL seperti berikut:

```
https://b15.[redacted.com]/file.php?spaceid=user@mail.com&file=filename.jpg
```

Pada awalnya, penulis mengira URL tersebut memiliki kerentanan LFI atau LFD, namun setelah dicoba merubah parameter `file` dengan file lain, ternyata tidak bekerja dan memunculkan pesan error.

```
https://b15.[redacted.com]/file.php?spaceid=user@mail.com&file=../../../../etc/passwd
```

![XSS on Error Page](/images/xss_error1_rev.png)

Namun bila di perhatikan, isi dari parameter `file` tadi terefleksi pada halaman error tersebut. Lalu penulis mencoba untuk memasukan tag HTML untuk mengetes apakah terdapat filter atau tidak pada parameter `file` tadi.

Dan benar saja, tag HTML berhasil dirender pada halaman tersebut.

```
https://b15.[redacted.com]/file.php?spaceid=user@mail.com&file=<h1>asu
```

![XSS on Error Page](/images/xss_error2_rev.png)

Tanpa menunggu lama, penulis langsung mencoba payload XSS pada halaman tersebut dan XSS pun tereksekusi.

```
https://b15.[redacted.com]/file.php?spaceid=user@mail.com&file=<img src=x onerror=alert(1)>
```

![XSS on Error Page](/images/xss_error3_rev.png)

Salah satu tips untuk `berburu` Reflected XSS adalah dengan mengetes berbagai parameter yang terdapat pada suatu `endpoint`. Baik yang ada pada halaman Front End, atau bahkan pada halaman Error seperti contoh diatas.

Demikian artikel ini ditulis, semoga bermanfaat bagi kita semua.
