---
layout: post
title: Bukalapak Email Disclosure via IDOR
date: 2018-12-10 13:37:00 PM
permalink: /project/2018-12-10-bukalapak-email-disclosure-via-idor/
author: xhellc0de
categories: [ pentesting, idor ]
tags: vulnerability idor
comments: true
image: /images/bl_banner.jpg
---
![Bukalapak](/images/bl_banner.jpg)

Bug ini ditemukan pada salah satu layanan Bukalapak. Pada layanan tersebut, terdapat sebuah fitur untuk meng-invite member Bukalapak untuk menjadi _anggota_ kita. Ketika melakukan proses invivation, saya pun membuka Burpsuite dan mengecek bagaimana bentuk data yang dikirimkan ke server.

Ternyata, data yang dikirimkan berisi parameter **bukalapak_user_id** yang merupakan user id dari member bukalapak, dan pada response-nya, menampilkan string **email** yang merupakah alamat email dari member tersebut.

**REQUEST**

![Bukalapak](/images/bl_idor_post.png)

**RESPONSE**

![Bukalapak](/images/bl_idor_response2.png)

Dengan merubah-rubah parameter **bukalapak_user_id**, kita bisa mendapatkan informasi alamat email dari setiap member Bukalapak.

![Bukalapak IDOR](/images/ezgif-1-5f5db7e9d815.gif)

```
Timeline:

[ 23 Nov 2018 ]: Bug Report
[ 23 Nov 2018 ]: Respon dari Bukalapak, Report Valid S3
[ 28 Nov 2018 ]: Bug Fixed
[ 28 Nov 2018 ]: Bukalapak Meminta Informasi untuk Reward
[ 10 Des 2018 ]: Reward Diterima
```
